package gg.artifice.bettermusic;

import net.minecraft.init.Blocks;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import org.apache.logging.log4j.Logger;

@Mod(modid = BetterMusic.MODID, name = BetterMusic.NAME, version = BetterMusic.VERSION)
public class BetterMusic
{
    public static final String MODID = "bettermusic";
    public static final String NAME = "Better Music";
    public static final String VERSION = "1.0";

    private static Logger logger;

    
    @EventHandler public void preInit(FMLPreInitializationEvent event)
    {
        logger = event.getModLog();
    }

    
    @EventHandler public void init(FMLInitializationEvent event)
    {
    	
    }
}
